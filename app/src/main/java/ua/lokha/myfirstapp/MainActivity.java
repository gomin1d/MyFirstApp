package ua.lokha.myfirstapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainActivity extends AppCompatActivity  {

    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = findViewById(R.id.text);

        findViewById(R.id.clear).setOnClickListener(view -> {
            text.setText("");
        });


        this.registerNumberListener();
        this.registerOperatorListener();

        this.registerResult();
    }

    private void registerResult() {
        findViewById(R.id.result).setOnClickListener(view -> {
            String data = text.getText().toString();
            String[] numbers = data.split("[/*+\\-]");
            List<BigDecimal> list = new ArrayList<>();
            List<Character> operators = new ArrayList<>();
            for (char c : data.toCharArray()) {
                if (Utils.hasOperator(c)) {
                    operators.add(c);
                }
            }
            for (String number : numbers) {
                list.add(new BigDecimal(number));
            }

            BigDecimal decimal = calculate(operators, list);

            text.setText(decimal.toString());
        });
    }

    private BigDecimal calculate(List<Character> characters, List<BigDecimal> numbers) {

        while (numbers.size() > 1) {
            int pos = 0;
            for (int i = 0; i < characters.size(); i++) {
                Character op = characters.get(i);
                if (op == '/' || op == '*') {
                    pos = i;
                    break;
                }
            }

            Character op = characters.remove(pos);
            BigDecimal var1 = numbers.get(pos);
            BigDecimal var2 = numbers.remove(pos + 1);
            switch (op) {
                case '+':
                    numbers.set(pos, var1.add(var2));
                    break;
                case '-':
                    numbers.set(pos, var1.add(var2.negate()));
                    break;
                case '/':
                    numbers.set(pos, var1.divide(var2, 10, BigDecimal.ROUND_FLOOR));
                    break;
                case '*':
                    numbers.set(pos, var1.multiply(var2));
                    break;
            }
        }

        return numbers.get(0);
    }

    private void registerOperatorListener() {
        View.OnClickListener numberEvent = view -> {
            CharSequence text = this.text.getText();
            if (text.length() > 0) {
                boolean add = Utils.hasNumber(text.charAt(text.length() - 1));

                char op = 0;
                switch (view.getId()) {
                    case R.id.opPlus:
                        op = '+';
                        break;
                    case R.id.opDiv:
                        op = '/';
                        break;
                    case R.id.opMinus:
                        op = '-';
                        break;
                    case R.id.opMult:
                        op = '*';
                        break;
                }

                if (add) {
                    this.text.append(Character.toString(op));
                } else {
                    char[] chars = this.text.getText().toString().toCharArray();
                    chars[chars.length - 1] = op;
                    this.text.setText(new String(chars));
                }
            }
        };

        findViewById(R.id.opDiv).setOnClickListener(numberEvent);
        findViewById(R.id.opMinus).setOnClickListener(numberEvent);
        findViewById(R.id.opMult).setOnClickListener(numberEvent);
        findViewById(R.id.opPlus).setOnClickListener(numberEvent);
    }

    private void registerNumberListener() {
        View.OnClickListener numberEvent = view -> {
            switch (view.getId()) {
                case R.id.num1:
                    text.append("1");
                    break;
                case R.id.num2:
                    text.append("2");
                    break;
                case R.id.num3:
                    text.append("3");
                    break;
                case R.id.num4:
                    text.append("4");
                    break;
                case R.id.num5:
                    text.append("5");
                    break;
                case R.id.num6:
                    text.append("6");
                    break;
                case R.id.num7:
                    text.append("7");
                    break;
                case R.id.num8:
                    text.append("8");
                    break;
                case R.id.num9:
                    text.append("9");
                    break;
            }
        };

        findViewById(R.id.num1).setOnClickListener(numberEvent);
        findViewById(R.id.num2).setOnClickListener(numberEvent);
        findViewById(R.id.num3).setOnClickListener(numberEvent);
        findViewById(R.id.num4).setOnClickListener(numberEvent);
        findViewById(R.id.num5).setOnClickListener(numberEvent);
        findViewById(R.id.num6).setOnClickListener(numberEvent);
        findViewById(R.id.num7).setOnClickListener(numberEvent);
        findViewById(R.id.num8).setOnClickListener(numberEvent);
        findViewById(R.id.num9).setOnClickListener(numberEvent);

        findViewById(R.id.num0).setOnClickListener(view -> {
            this.text.append("0");
        });
    }
}
