package ua.lokha.myfirstapp;

/**
 * Created by lokha on 21.02.2018.
 */

public class Utils {

    public static boolean hasNumber(char c) {
        switch (c) {
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case '0':
                return true;
            default:
                return false;
        }
    }

    public static boolean hasOperator(char c) {
        switch (c) {
            case '-':
            case '+':
            case '*':
            case '/':
                return true;
            default:
                return false;
        }
    }
}
